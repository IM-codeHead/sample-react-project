## Prerequisites
- Node 8.1 and above
- npm 5.5 and above

## Getting Started

To get started, clone this repo, cd into the new repo and do the following in both repos /app and /node-server :

```
    npm install
    npm start
```

A web server should start on localhost:8080. Any change made to files in the project will automatically rebuild / refresh the page for the app.
