import React from "react";
import ReactDOM from "react-dom";

const TableCell = (props) => {
  return (
    <td>
      {props.value}
    </td>
  );
};

export default TableCell;
