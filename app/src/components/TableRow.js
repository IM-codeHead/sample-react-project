import React from "react";
import ReactDOM from "react-dom";
import TableCell from "./TableCell";

const createColumns = (data, rowIndex) => {
  const columns = [];
  Object.keys(data).forEach((cellData, index) => {
    columns.push(<TableCell value={data[cellData]} key={`cell_${rowIndex}_${index}`}/>);
  });
  return columns;
}

const TableRow = (props) => {
  return (
    <tr>
      {createColumns(props.columnData, props.rowIndex)}
    </tr>
  );
};

export default TableRow;
