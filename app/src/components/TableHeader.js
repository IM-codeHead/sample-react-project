import React from "react";
import ReactDOM from "react-dom";

const createHeader = (data) => {
  const columns = [];
  data.forEach((cellData, index) => {
    columns.push(<th key={`th_${index}`}>{cellData}</th>);
  });
  return columns;
}

const TableRow = (props) => {
  return (
    <tr className={'header-row'}>
      {createHeader(props.headerData)}
    </tr>
  );
};

export default TableRow;
