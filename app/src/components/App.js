import React from "react";
import ReactDOM from "react-dom";
import TableRow from "./TableRow";
import TableHeader from "./TableHeader";

const headerData = ['Contact Name', 'Country', 'Email Id', 'Company Name', 'Plan Type', 'Trial Expired', 'Trial Extneded'];

const createTable = (contacts) => {
  const rows = [];
  // Mapping only required field
  const tableData = contacts.map((data) => {
    const newObj = {
      contactName: data.contact_name,
      country: data.country,
      email: data.email,
      name: data.name,
      planName: data.plan_name,
      trialExpired: data.is_trial_expired ? 'YES' : 'NO',
      trialExtended: data.is_trial_period_extended ? 'YES' : 'NO',
    };
    return newObj;
  });

  // Creating rows as per contact details
  tableData.forEach((details, index) => {
    rows.push(
      <TableRow columnData={details} rowIndex={index} key={`row_${index}`}/>
    );
  });
  return rows;
};

export default class App extends React.Component {
  constructor(props) {
    super();
    this.state = {
      contacts: props.tableData,
    };
  }

  updateContacts(contacts) {
    this.setState({ contacts });
  }

  render() {
    return (
      <table>
        <tbody>
          <TableHeader headerData={headerData} />
          {this.state.contacts ? createTable(this.state.contacts) : <tr><td>{'Loading Contacts...'}</td></tr>}
        </tbody>
      </table>
    );
  }
}
