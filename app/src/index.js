import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

const xhr = new XMLHttpRequest();
let tableData = null;
let appRef = null;

// Sending request to node server
xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    tableData = JSON.parse(this.responseText).organizations;
    appRef.updateContacts(tableData)
  }
});

xhr.open("GET", "http://localhost:3000");

xhr.send(null);

ReactDOM.render(<App ref={(ref) => { appRef = ref }}tableData={tableData} />, document.getElementById("app"));
