const express = require('express');
const http = require('http');
const request = require('request');

const hostname = 'localhost';
const port = 3000;

const options = {
  "url": "https://books.zoho.com/api/v3/organizations?organization_id=649249007",
  "headers": {
    "authorization": "Zoho-authtoken db36e02a50b57e081efe533a8a0f834b",
    "cache-control": "no-cache"
  }
};

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/', (req, res) => {
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res.end(body);
    }
  });
});

const server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
